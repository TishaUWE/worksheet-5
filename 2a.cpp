#include <iostream>
#include <fstream>
#include<cstring>
using namespace std;

struct Employee {
    char name[100];
    int tax_number;
    float salary;
};

int main() {
    Employee employees[100];
    int i = 0;
    string name_input;
    bool check = false;

    while (!check) {
        cout << "Enter name: ";
        cin >> name_input;

        if (name_input == "fin") {
            check = true;
        } else {
            strcpy(employees[i].name, name_input.c_str());
            cout << "Enter tax number: ";
            cin >> employees[i].tax_number;
            cout << "Enter salary: ";
            cin >> employees[i].salary;

            i++;
        }
    }

    ofstream binaryFile("test.bin", ios::binary);
    binaryFile.write(reinterpret_cast<char*>(employees), sizeof(Employee) *i);
    binaryFile.close();

    return 0;
}
