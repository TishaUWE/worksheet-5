#include <iostream>
#include <fstream>
#include <string>
using namespace std;

struct Employee {
    char name[100];
    int tax_number;
    float salary;
};

int main() {
    ifstream binaryFile("test.bin", ios::binary);
    Employee employees[100];
    int employeeCount = 0;
    float new_salary;
    int total = 0;
    int count = 0;

    cout << "Enter salary: ";
    cin >> new_salary;

    binaryFile.read(reinterpret_cast<char*>(employees), sizeof(Employee) * 100);
    binaryFile.close();

    ofstream dataFile("data.bin", ios::binary);
    for (int i = 0; i < 100; i++) {
        if (employees[i].salary > new_salary) {
            dataFile.write(reinterpret_cast<char*>(&employees[i]), sizeof(Employee));
            total += employees[i].salary;
         count++;
        }
    }
    dataFile.close();

    if  (count > 0) {
        cout << "Average salary: " << total / count << endl;
    }
    else {
        cout << "The required condition was not met." << endl;
    }

    return 0;
}
